﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.DAL.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Intention.BLL.Implements
{
    public class AccountService : IAccountService
    {
        private readonly IBaseRepository baseRepository;
        private readonly string SecretKey = "bXkgbmFtZSBpcyBzdXJhc2FrIGhhbmxhbXl1YW5n";
        public AccountService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public UserToken Authenticate(string G_ID)  
        {
            string sql = $@"SELECT [CU_ID] AS Id
                             FROM [Customer]
                             WHERE [Customer].[G_ID] = '{G_ID}'";

            var res = baseRepository.Query<UserToken>(sql).SingleOrDefault();

            if(res == null)
            {
                return null;
            }
            else
            {
                //Genarate Token Jwt
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(SecretKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name,res.G_ID.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                res.Token = tokenHandler.WriteToken(token);
                res.Expired = tokenDescriptor.Expires.ToString();

                return res;
            }
        }

        public string Register(ProfileDto profileDto)
        {
            string sql = $@"INSERT INTO [Customer]
                            ([G_ID],[Name],[Email],[Image_Path],[Address])
                            VALUES ( '{profileDto.G_ID}'
                                    ,'{profileDto.Name}'
                                    ,'{profileDto.Email}'
                                    ,'{profileDto.Image_Path}'
                                    ,'{profileDto.Address}')";
            var res = baseRepository.Execute<int>(sql);

            return res != 0 ? "Register Success" : "Register UnSuccess";
            
        }
    }
}
