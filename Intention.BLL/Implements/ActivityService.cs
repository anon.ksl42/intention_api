﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intention.BLL.Implements
{
    public class ActivityService : IActivityService
    {
        private readonly IBaseRepository baseRepository;
        public ActivityService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public List<ActivityDto> GetActivity( int CH_ID,int CU_ID)
        {
            string sql = $@"SELECT [Activity].[Act_ID] AS Act_ID
                                   ,[Activity].[Challenge_Id] AS Challenge_Id
                                   ,[Activity].[UpdateDate] AS UpdateDate
                                   ,[Activity].[Description] AS Description
                                   ,[Challenge].[ImageCover] AS Image_Path
                             FROM [Activity]
                             INNER JOIN [Challenge] ON [Activity].Challenge_Id = Challenge.CH_ID
                             WHERE [Activity].[Challenge_Id] = {CH_ID} AND [Activity].[CustomerId] = {CU_ID}
                             ORDER BY [Activity].[Act_ID] DESC";

            
            return baseRepository.Query<ActivityDto>(sql).ToList();
        }

        public string UpdateActivity(ActivityDto activity)
        {
            activity.UpdateDate = DateTime.Now;
            string sql1 = $@"INSERT INTO [Activity]
                           ([Description],[UpdateDate],[Challenge_Id],[CustomerId])
                           VALUES ('{activity.Description}','{activity.UpdateDate}',{activity.Challenge_Id},{activity.CustomerId})";
            var insertData1 = baseRepository.Execute<int>(sql1);

            string sql3 = $@"SELECT [Activity].[Act_ID] AS Act_ID
                                   ,[Activity].[Challenge_Id] AS Challenge_Id
                             FROM [Activity]
                             Where [Activity].[Challenge_Id] = {activity.Challenge_Id} AND [Activity].[CustomerId] = {activity.CustomerId}";

            var data = baseRepository.Query<ActivityDto>(sql3).FirstOrDefault();

            string sql2 = $@"INSERT INTO [Images]
                           ([Act_ID],[Image_Path])
                           VALUES ({data.Act_ID},'{activity.Image_Path}')";

            var insertData2 = baseRepository.Execute<int>(sql2);

           

            if (insertData1 != 0 && insertData2 != 0 )
                return "Insert activity success.";
            else
                return "Insert activity unsuccess.";
        }
    }
}
