﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intention.BLL.Implements
{
    public class ProfileService : IProfileService
    {

        private readonly IBaseRepository baseRepository;
        public ProfileService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public string EditProfile(ProfileDto profileDto)
        {
            string sql = $@"UPDATE [Customer]
                            SET [Customer].[Name] = '{profileDto.Name}'
                               ,[Customer].[Email] = '{profileDto.Email}'
                               ,[Customer].[Address] = '{profileDto.Address}'
                            WHERE [Customer].[CU_ID] = {profileDto.CU_ID}";

            var result = baseRepository.Execute<int>(sql);

            if (result != 0)
                return "Edit profile success";
            else
                return "Edit profile unsuccess";
        }

        public ProfileDto GetProfile(int id)
        {
            string sql = $@"SELECT [Customer].[CU_ID] AS CU_ID
                                  ,[Customer].[Name]  AS Name
                                  ,[Customer].[Email] AS Email
                                  ,[Customer].[Image_Path] AS Image_Path
                                  ,[Customer].[Address] AS Address
                             FROM [Customer] WHERE [Customer].[CU_ID] = {id}";

            return baseRepository.Query<ProfileDto>(sql).FirstOrDefault();

        }

        public string UploadProfileImage(ProfileDto profileDto)
        {
            string sql = $@"UPDATE [Customer]
                            SET [Customer].[Image_Path] = '{profileDto.Image_Path}'
                            WHERE [Customer].[CU_ID] = {profileDto.CU_ID}";

            var result = baseRepository.Execute<int>(sql);

            if (result != 0)
                return "Img profile success";
            else
                return "Img profile unsuccess";
        }
    }
}
