﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intention.BLL.Implements
{
    public class ChallengeService : IChallengeService
    {
        private readonly IBaseRepository baseRepository;

        public ChallengeService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public string CreateChallenge(ChallengeDto challenge)
        {
            challenge.Status = "1";
            challenge.PartyList = 0;
            string sql = $@"INSERT INTO [Challenge]
                            ([ImageCover],[Title],[Customer_Id],[PartyList],[Weight],[Date_End],[Date_Start],[Status])
                            VALUES ('{challenge.ImageCover}'
                                    ,'{challenge.Title}'
                                    , {challenge.Customer_Id}
                                    , {challenge.PartyList}
                                    , {challenge.Weight}
                                    ,'{challenge.Date_End}'
                                    ,'{challenge.Date_Start}'
                                    ,'{challenge.Status}')";
            var result = baseRepository.Execute<int>(sql);

            if (result != 0)
                return "Create challenge success.";
            else
                return "Create challenge unsuccess.";
        }

        public string DeleteChallenge(int id)
        {
            string sql = $@"UPDATE [Challenge]
                            SET [Challenge].[Status] = '{0}',[Challenge].[Delete_Date] = '{DateTime.Now}'
                            WHERE [Challenge].[CH_ID] = {id}";

            var result = baseRepository.Execute<int>(sql);

            if(result != 0)
                return "Delete challenge success";
            else
                return "Delete challenge unsuccess";
        }

        public ChallengeDto DetailChallenge(int ch_id)
        {
            string sql = $@"SELECT [Challenge].[CH_ID] AS CH_ID
                                  ,[Challenge].[ImageCover] AS ImageCover
                                  ,[Challenge].[PartyList] AS PartyList
                                  ,[Challenge].[Date_Start] AS Date_Start
                                  ,[Challenge].[Date_End] AS Date_End
                                  ,[Challenge].[Title] AS Title
                                  ,[Customer].[Name] AS NameCus
                                  ,[Customer].[Image_Path] AS Image_Path
                             FROM [Challenge] 
                             INNER JOIN [Customer] ON [Challenge].Customer_Id = Customer.CU_ID
                             WHERE [Challenge].[CH_ID] = {ch_id}";

            var data = baseRepository.Query<ChallengeDto>(sql).FirstOrDefault();

            string sql2 = $@"SELECT [Party].CU_ID AS CU_ID
								   ,[Customer].Image_Path AS Image_Path
                             FROM [Challenge] 
                             INNER JOIN [Customer] ON [Challenge].Customer_Id = Customer.CU_ID
                             INNER JOIN [Party] ON [Challenge].CH_ID = Party.CH_ID
                             WHERE [Challenge].[CH_ID] = {ch_id}";

            var data2 = baseRepository.Query<UserJoin>(sql2).ToList();

            data.UserJoin = data2;
            

            if (data != null)
                return data;
            else
                return null;
        }

        public List<ChallengeDto> GetChallenge()
        {
            string sql = $@"SELECT [Challenge].[CH_ID] AS CH_ID
                                  ,[Challenge].[ImageCover] AS ImageCover
                                  ,[Challenge].[Title] AS Title
                                  ,[Customer].[Name] AS NameCus
                                  ,[Customer].[Image_Path] AS Image_Path
                            FROM [Challenge]
                            INNER JOIN [Customer] ON [Challenge].Customer_Id = Customer.CU_ID
                            where [Status] = '{1}'
                            ORDER BY [Challenge].[CH_ID] DESC
                            OFFSET {0} ROWS
                            FETCH NEXT {10} ROWS ONLY";

            return baseRepository.Query<ChallengeDto>(sql).ToList();
        }

        public List<ChallengeDto> GetUpdataChallenge(int cu_id)
        {
            string sql = $@"SELECT [Challenge].[CH_ID] AS CH_ID
                                  ,[Challenge].[Title] AS Title
                                  ,[Challenge].[Date_Start] AS Date_Start
                                  ,[Challenge].[Date_End] AS Date_End
                            FROM [Challenge]
                            INNER JOIN [Customer] ON [Challenge].Customer_Id = Customer.CU_ID
                            where [Status] = '{1}' AND [Customer].CU_ID = {cu_id}
                            ORDER BY [Challenge].[CH_ID] DESC
                            OFFSET {0} ROWS
                            FETCH NEXT {10} ROWS ONLY";

            return baseRepository.Query<ChallengeDto>(sql).ToList();
        }

        public string JoinChallenge(int cu_id, int ch_id)
        {
            string sql = $@"INSERT INTO [Party]
                           ([CU_ID],[CH_ID],[Join_Date])
                           VALUES ({cu_id},{ch_id},'{DateTime.Now}')";

            var result = baseRepository.Execute<int>(sql);

            string sql3 = $@"SELECT [Challenge].[PartyList]
                             FROM [Challenge] WHERE [Challenge].[CH_ID] = {ch_id}";

            var data = baseRepository.Query<ChallengeDto>(sql3).FirstOrDefault();

            string sql4 = $@"UPDATE [Challenge]
                            SET [Challenge].[PartyList] = {data.PartyList+1}
                            WHERE [Challenge].[CH_ID] = {ch_id}";

            var result1 = baseRepository.Execute<int>(sql4);

            if(result != 0 && result1 != 0)
                return "Join success";
            else
                return "Join unsuccess";
        }

        public List<ChallengeDto> Timeline(int cu_id)
        {
            string sql = $@"SELECT [Challenge].[CH_ID] AS CH_ID
                                  ,[Challenge].[Date_Start] AS Date_Start
                                  ,[Challenge].[Date_End] AS Date_End
                                  ,[Challenge].[Title] AS Title
                                  ,[Challenge].[Status] AS Status
                             FROM [Challenge] 
                             INNER JOIN [Customer] ON [Challenge].Customer_Id = Customer.CU_ID
                             WHERE [Challenge].[Customer_Id] = {cu_id}";

            return baseRepository.Query<ChallengeDto>(sql).ToList();
        }
    }
}
