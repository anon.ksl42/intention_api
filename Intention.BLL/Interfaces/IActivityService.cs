﻿using Intention.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Interfaces
{
    public interface IActivityService
    {
        string UpdateActivity (ActivityDto activity);

        List<ActivityDto> GetActivity(int CH_ID, int CU_ID);
    }
}
