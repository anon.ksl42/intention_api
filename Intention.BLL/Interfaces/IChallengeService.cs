﻿using Intention.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Interfaces
{
    public interface IChallengeService
    {

        List<ChallengeDto> GetChallenge();
        string CreateChallenge(ChallengeDto challenge);
        ChallengeDto DetailChallenge(int ch_id);
        List<ChallengeDto> Timeline(int cu_id);
        List<ChallengeDto> GetUpdataChallenge(int cu_id);
        string JoinChallenge(int cu_id,int ch_id);
        string DeleteChallenge(int id);

    }
}
