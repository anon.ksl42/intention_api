﻿using Intention.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Interfaces
{
    public interface IProfileService
    {

        ProfileDto GetProfile(int id);
        string EditProfile(ProfileDto profileDto); 
        string UploadProfileImage(ProfileDto profileDto); 
    }
}
