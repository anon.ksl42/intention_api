﻿using Intention.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Intention.BLL.Interfaces
{
    public interface IAccountService
    {
        string Register(ProfileDto profileDto);
        UserToken Authenticate(string G_ID);
    }
}
