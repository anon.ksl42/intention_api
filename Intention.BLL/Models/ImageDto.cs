﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Models
{
    public class ImageDto
    {
        public int Image_ID { get; set; }
        public int Act_ID { get; set; }
        public string Image_Path { get; set; }


    }
}
