﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Models
{
    public class UserToken
    {
        public int Id { get; set; }
        public string G_ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string Expired { get; set; }

    }
}
