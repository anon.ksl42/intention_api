﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Models
{
    public class ProfileDto
    {
        public int CU_ID { get; set; }
        public string G_ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Image_Path { get; set; }
        public string Address { get; set; }

    }
}
