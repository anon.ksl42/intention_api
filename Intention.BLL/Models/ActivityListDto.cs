﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Models
{
    public class ActivityListDto
    {
        public int Act_ID { get; set; }
        public int Challenge_Id { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Description { get; set; }
        public string Image_Path { get; set; }
    }
}
