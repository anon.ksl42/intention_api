﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.BLL.Models
{
    public class ChallengeDto
    {

        public int CH_ID { get; set; }
        public string ImageCover { get; set; }
        public string Image_Path { get; set; }
        public int PartyList { get; set; }
        public DateTime Update_Date { get; set; }
        public DateTime Date_Start { get; set; }
        public DateTime Date_End { get; set; }
        public DateTime Delete_Date { get; set; }
        public string Title { get; set; }
        public string NameCus { get; set; }
        public int Weight { get; set; }
        public int Customer_Id { get; set; }
        public string Status { get; set; }
        public List<UserJoin> UserJoin { get; set; }



    }
}
