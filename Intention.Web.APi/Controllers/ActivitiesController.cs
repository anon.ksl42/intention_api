﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.Web.APi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Controllers
{
    [Authorize]
    [Route("/api/v1/[controller]")]
    public class ActivitiesController : Controller
    {
        private readonly string UrlImageGoals = "https://localhost:44325/ImgGoals/";
        private readonly IActivityService activityService;

        public ActivitiesController(IActivityService activityService)
        {
            this.activityService = activityService;
        }
        [HttpGet("{CH_ID}/{CU_ID}")]
        public IActionResult GetActivity([FromRoute] int CH_ID,int CU_ID)
        {
            if (CH_ID != 0)
            {
                try
                {
                    var result = activityService.GetActivity(CH_ID,CU_ID);
                    var data = result.Select(x => new ActivityListModel
                    {
                        Act_ID = x.Act_ID,
                        Challenge_Id = x.Challenge_Id,
                        Description = x.Description,
                        UpdateDate = x.UpdateDate.ToShortDateString(),
                        Image_Path = x.Image_Path

                    });

                    return Ok(data);
                }
                catch (Exception e)
                {

                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
                return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }
        }
        // /api/v1/activitys
        [HttpPost]
        public IActionResult PostUpdateActivity([FromForm] ActivityModel activity)
        {
            if (string.Equals(activity.Image.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(activity.Image.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(activity.Image.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)
                )
            {
                try
                {
                    if (!string.IsNullOrEmpty(activity.Description)
                        && !string.IsNullOrEmpty(activity.Description)
                        && activity.Challenge_Id != 0
                        && activity.Image != null
                        )
                    {
                        string pathImage = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\ImgGoals\" + activity.Image.FileName);
                        using (var stream = new FileStream(pathImage, FileMode.Create))
                        {
                            activity.Image.CopyTo(stream);
                        }

                        ActivityDto activityDto = new ActivityDto();
                        activityDto.Image_Path = UrlImageGoals + activity.Image.FileName;
                        activityDto.Challenge_Id = activity.Challenge_Id;
                        activityDto.Description = activity.Description;
                        activityDto.CustomerId = activity.CustomerId;

                        var result = activityService.UpdateActivity(activityDto);

                        return Ok(result);

                    }
                    else
                    {
                        return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
                    }


                }
                catch (Exception e)
                {

                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
                return new BadRequestObjectResult(new {StateCode = 400 , Error = "Not image type"});
            }
        }
    }
}
