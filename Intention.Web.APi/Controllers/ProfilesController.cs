﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.Web.APi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    public class ProfilesController : Controller
    {
        private readonly IProfileService profileService;
        private readonly string UrlImageProfile = "http://localhost:63925/ImgProfiles/";
        public ProfilesController(IProfileService profileService)
        {
            this.profileService = profileService;
        }

        [HttpGet]
        public IActionResult GetProfile([FromQuery] ProfileModel profileModels)
        {
            if (profileModels.CU_ID != 0)
            {
                try
                {
                    var result = profileService.GetProfile(profileModels.CU_ID);
                    if (result != null)
                        return Ok(result);
                    else
                        return new OkObjectResult(new { StateCode = 400, Error = "No data" });
                }
                catch (Exception e)
                {
                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
                return new OkObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }
        }

        [HttpPut("{CU_ID}/Image")]
        public IActionResult PutImgProfile([FromForm] ProfileModel profileModel)
        {
            if (profileModel.Image != null &&
                string.Equals(profileModel.Image.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(profileModel.Image.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(profileModel.Image.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)
               )
            {
                try
                {

                    string pathImage = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\ImgProfiles\" + profileModel.Image.FileName);
                    using (var stream = new FileStream(pathImage, FileMode.Create))
                    {
                        profileModel.Image.CopyTo(stream);
                    }

                    ProfileDto profileDto = new ProfileDto();
                    profileDto.Image_Path = UrlImageProfile + profileModel.Image.FileName;
                    profileDto.CU_ID = profileModel.CU_ID;

                    var result = profileService.UploadProfileImage(profileDto);
                    return Ok(result);
                }
                catch (Exception e)
                {
                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
                return new OkObjectResult(new { StateCode = 400, Error = "Not image type" });
            }
        }

        [HttpPut("{CU_ID}")]
        public IActionResult PutProfile([FromBody] ProfileModel profileModel)
        {
            try
            {
                if(!string.IsNullOrEmpty(profileModel.Email) &&
                   !string.IsNullOrEmpty(profileModel.Address) &&
                   !string.IsNullOrEmpty(profileModel.Name) &&
                   profileModel.CU_ID != 0)
                {
                    ProfileDto profileDto = new ProfileDto();
                    profileDto.CU_ID = profileModel.CU_ID;
                    profileDto.Name = profileModel.Name;
                    profileDto.Email = profileModel.Email;
                    profileDto.Address = profileModel.Address;

                    var result = profileService.EditProfile(profileDto);
                    return Ok(result);
                }
                else
                {
                    return new OkObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
                }
                
            }
            catch (Exception e)
            {
                return new OkObjectResult(new { StateCode = 502, Error = e.Message });
            }
        }
    }
}
