﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.Web.APi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Controllers
{
    [Route("api/v1/[controller]")]
    public class AccountsController : Controller
    {
        private readonly IAccountService accountService;

        public AccountsController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody]string G_ID)
        {
            if (!string.IsNullOrEmpty(G_ID))
            {
                try
                {
                    var res = accountService.Authenticate(G_ID);

                    if (res == null)
                        return NoContent();


                    return new OkObjectResult(new { res.Id, res.Token, res.Expired });
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }
        }

        [HttpPost("Register")]
        public IActionResult PostRegister([FromBody] ProfileModel profileModel)
        {
            if (!string.IsNullOrEmpty(profileModel.G_ID) &&
                !string.IsNullOrEmpty(profileModel.Name) &&
                !string.IsNullOrEmpty(profileModel.Image_Path) &&
                !string.IsNullOrEmpty(profileModel.Email) &&
                !string.IsNullOrEmpty(profileModel.Address))
            {
                try
                {
                    ProfileDto profileDto = new ProfileDto();
                    profileDto.G_ID = profileModel.G_ID;
                    profileDto.Name = profileModel.Name;
                    profileDto.Email = profileModel.Email;
                    profileDto.Image_Path = profileModel.Image_Path;
                    profileDto.Address = profileModel.Address;

                    var result = accountService.Register(profileDto);

                    return Ok(result);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            else
            {
                return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }

        }
    }
}
