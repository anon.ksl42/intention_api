﻿using Intention.BLL.Interfaces;
using Intention.BLL.Models;
using Intention.Web.APi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Controllers
{
    [Authorize]
    [Route("/api/v1/[controller]")]
    public class ChallengesController : Controller
    {

        private readonly IChallengeService challengeService;
        private readonly string UrlImageFeed = "http://localhost:63925/ImgFeeds/";
        public ChallengesController(IChallengeService challengeService)
        {
            this.challengeService = challengeService;

        }

        
        [HttpGet]
        public IActionResult GetChallenge()
        {
            try
            {
                var result = challengeService.GetChallenge();
                if (result != null)
                {
                    var data = result.Select(x => new ChallengeNewModel()
                    {
                        CH_ID = x.CH_ID,
                        ImageCover = x.ImageCover,
                        Title = x.Title,
                        Image_Path = x.Image_Path,
                        NameCus = x.NameCus
                    });
                    return Ok(data);
                }
                else
                    return NoContent();
            }
            catch (Exception e)
            {

                return new OkObjectResult(new { StateCode = 502, Error = e.Message });
            }
        }

        [HttpGet("{CU_ID}")]
        public IActionResult GetUpdateChallenge([FromRoute] int CU_ID)
        {
            try
            {
                var result = challengeService.GetUpdataChallenge(CU_ID);
                if (result != null)
                {
                    var data = result.Select(x => new GetUpdateChallenge()
                    {
                        CH_ID = x.CH_ID,
                        Title = x.Title,
                        Date_Start = x.Date_Start.ToShortDateString(),
                        Date_End = x.Date_End.ToShortDateString()
                    });
                    return new OkObjectResult(data);
                }
                else
                    return new BadRequestObjectResult(new { StateCode = 400, Error = "No data" });
            }
            catch (Exception e)
            {

                return new OkObjectResult(new { StateCode = 502, Error = e.Message });
            }
        }


        // /api/v1/challenges
        [HttpPost]
        public IActionResult PostChallenge([FromForm] ChallengeModel challenge)
        {
            if (string.Equals(challenge.Image.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(challenge.Image.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(challenge.Image.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)
               )
            {
                try
                {
                    if (!string.IsNullOrEmpty(challenge.Title)
                       && challenge.Customer_Id != 0
                       && challenge.Weight != 0
                       && challenge.Image != null
                       )
                    {
                        string pathImage = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\ImgFeedCover\" + challenge.Image.FileName);
                        using (var stream = new FileStream(pathImage, FileMode.Create))
                        {
                            challenge.Image.CopyTo(stream);
                        }
                        
                        

                        ChallengeDto challengeDto = new ChallengeDto();
                        challengeDto.ImageCover = UrlImageFeed + challenge.Image.FileName;
                        challengeDto.Status = challenge.Status;
                        challengeDto.Title = challenge.Title;
                        challengeDto.Weight = challenge.Weight;
                        challengeDto.Date_Start = challenge.Date_Start;
                        challengeDto.Date_End = challenge.Date_End;
                        challengeDto.Customer_Id = challenge.Customer_Id;


                        var result = challengeService.CreateChallenge(challengeDto);

                        return Ok(result);
                    }
                    else
                    {
                        return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
                    }
                }
                catch (Exception e)
                {
                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
                return new BadRequestObjectResult(new { StateCode = 400, Error = "Not image type" });
            }
        }

        // /api/v1/challenges
        [HttpPut]
        public IActionResult JoinChallenge([FromQuery] JoinChallengeModel join)
        {
            if (join.CU_ID != 0 && join.CH_ID != 0)
            {
                try
                {
                    var result = challengeService.JoinChallenge(join.CU_ID, join.CH_ID);
                    return Ok(result);
                }
                catch (Exception e)
                {
                    return new OkObjectResult(new { StateCode = 502, Error = e.Message }); ;
                }
            }
            else
            {
                return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }

        }

        // /api/v1/challenges
        [HttpDelete]
        public IActionResult DeleteChallenge([FromQuery] JoinChallengeModel del)
        {
            if(del.CH_ID != 0)
            {
                try
                {
                    var result = challengeService.DeleteChallenge(del.CH_ID);
                    return Ok(result);
                }
                catch (Exception e)
                {
                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
               return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }
           
        }

        [HttpGet("{CH_ID}/Detail")]
        public IActionResult DetailChallenge([FromRoute] JoinChallengeModel del)
        {
            if (del.CH_ID != 0)
            {
                try
                {
                    var result = challengeService.DetailChallenge(del.CH_ID);
                    ChallengeDetailModel x = new ChallengeDetailModel();
                    x.CH_ID = result.CH_ID;
                    x.ImageCover = result.ImageCover;
                    x.Image_Path = result.Image_Path;
                    x.Date_Start = result.Date_Start.ToShortDateString();
                    x.Date_End = result.Date_End.ToShortDateString();
                    x.Title = result.Title;
                    x.NameCus = result.NameCus;
                    x.PartyList = result.PartyList;
                    x.UserJoin = result.UserJoin;

                    return Ok(x);
                }
                catch (Exception e)
                {
                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
                return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }

        }

        [HttpGet("{CU_ID}/Timeline")]
        public IActionResult getTimeline([FromRoute] JoinChallengeModel del)
        {
            if (del.CU_ID != 0)
            {
                try
                {
                    var result = challengeService.Timeline(del.CU_ID);
                    var data = result.Select(x => new ChallengeTimeline()
                    {
                        CH_ID = x.CH_ID,
                        Title = x.Title,
                        Date_Start = x.Date_Start.ToShortDateString(),
                        Date_End = x.Date_End.ToShortDateString(),
                        Status = x.Status
                    }) ;
                    return Ok(data);
                }
                catch (Exception e)
                {
                    return new OkObjectResult(new { StateCode = 502, Error = e.Message });
                }
            }
            else
            {
                return new BadRequestObjectResult(new { StateCode = 400, Error = "Parameter is not valid" });
            }

        }
    }
}
