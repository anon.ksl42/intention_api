﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Models
{
    public class ProfileModel
    {

        public int CU_ID { get; set; }
        public string G_ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Image_Path { get; set; }
        public string Address { get; set; }
        public IFormFile Image { get; set; }
    }
}
