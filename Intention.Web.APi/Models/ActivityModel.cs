﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Models
{
    public class ActivityModel
    {
        public int Act_ID { get; set; }
        public int Challenge_Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Description { get; set; }
        public string Image_Path { get; set; }
        public IFormFile Image { get; set; }
    }
}
