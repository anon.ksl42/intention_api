﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Models
{
    public class ChallengeNewModel
    {
        public int CH_ID { get; set; }
        public string ImageCover { get; set; }
        public string Image_Path { get; set; }
        public string Title { get; set; }
        public string NameCus { get; set; }

    }
}
