﻿using Intention.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Models
{
    public class ChallengeDetailModel
    {
        public int CH_ID { get; set; }
        public string ImageCover { get; set; }
        public string Image_Path { get; set; }
        public string Title { get; set; }
        public string NameCus { get; set; }
        public int PartyList { get; set; }
        public List<UserJoin> UserJoin { get; set; }
        public string Date_Start { get; set; }
        public string Date_End { get; set; }
    }
}
