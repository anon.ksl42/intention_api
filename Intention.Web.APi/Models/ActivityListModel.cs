﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Models
{
    public class ActivityListModel
    {
        public int Act_ID { get; set; }
        public int Challenge_Id { get; set; }
       
        public string UpdateDate { get; set; }
        public string Description { get; set; }
        public string Image_Path { get; set; }
    }
}
