﻿using Intention.BLL.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Models
{
    public class ChallengeModel
    {
        public int CH_ID { get; set; }
        public string ImageCover { get; set; }
        public string Image_Path { get; set; }
        public int PartyList { get; set; }
        public Nullable<DateTime> Update_Date { get; set; }
        public DateTime Date_Start { get; set; }
        public DateTime Date_End { get; set; }
        public Nullable<DateTime> Delete_Date { get; set; }
        public string Title { get; set; }
        public int Weight { get; set; }
        public int Customer_Id { get; set; }
        public string Status { get; set; }
        public List<UserJoin> UserJoin { get; set; }
        public IFormFile Image { get; set; }
    }
}
