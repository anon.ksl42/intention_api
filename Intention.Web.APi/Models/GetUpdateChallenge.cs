﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intention.Web.APi.Models
{
    public class GetUpdateChallenge
    {
        public int CH_ID { get; set; }
        public string Title { get; set; }
        public string Date_Start { get; set; }
        public string Date_End { get; set; }
    }
}
