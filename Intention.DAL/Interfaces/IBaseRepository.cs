﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intention.DAL.Interfaces
{
    public interface IBaseRepository
    {

        IEnumerable<T> Query<T>(string sql);

        int Execute<T>(string sql);
    }
}
