﻿using Dapper;
using Intention.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Intention.DAL.Implements
{
    public class BaseRepository : IBaseRepository
    {
        private readonly string ConnectionString;

        public BaseRepository(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }


        private T WithConnection<T> (Func<IDbConnection,T> getData)
        {
            try
            {
                using(var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    return getData(connection);
                }
            }
            catch (SqlException e)
            {

                throw e;
            }
        }



        public int Execute<T>(string sql)
        {
            return WithConnection(c => c.Execute(sql));
        }

        public IEnumerable<T> Query<T>(string sql)
        {
            return WithConnection(c => c.Query<T>(sql));
        }
    }
}
